package ms

import (
	"context"
	"database/sql"
	"errors"
)

var (
	ErrNotQueryStmtPair         = errors.New("ms: Not query and stmt pair")
	ErrOnlyOneRowMustBeAffected = errors.New("ms: Only one row must be affect")
)

type RowReaderFunc func() (item interface{}, scanArgs []interface{})

//type ScanArgsFunc func(interface{}) []interface{}

func PrepareContextQueriesStmtPairs(ctx context.Context, db *sql.DB, queryAndStmtPairs ...interface{}) error {
	l := len(queryAndStmtPairs)
	if l%2 != 0 {
		return ErrNotQueryStmtPair
	}
	for i := 0; i < l; i += 2 {
		if _, ok := queryAndStmtPairs[i].(string); !ok {
			return ErrNotQueryStmtPair
		}
		if _, ok := queryAndStmtPairs[i+1].(**sql.Stmt); !ok {
			return ErrNotQueryStmtPair
		}
	}

	for i := 0; i < l; i += 2 {
		query, _ := queryAndStmtPairs[i].(string)
		stmt, _ := queryAndStmtPairs[i+1].(**sql.Stmt)
		var err error
		*stmt, err = db.PrepareContext(ctx, query)
		if err != nil {
			return err
		}
	}

	return nil
}

func Insert(ctx context.Context, tx *sql.Tx, stmt *sql.Stmt, args ...interface{}) error {
	return execRow(ctx, tx, stmt, args...)
}

func SelectRow(ctx context.Context, tx *sql.Tx, stmt *sql.Stmt, rr RowReaderFunc, args ...interface{}) (interface{}, error) {
	p, scanArgs := rr()
	r := tx.Stmt(stmt).QueryRowContext(ctx, args...)
	err := r.Scan(scanArgs...)
	if err != nil {
		return nil, err
	}
	return p, nil
}

func SelectExists(ctx context.Context, tx *sql.Tx, stmt *sql.Stmt, args ...interface{}) (bool, error) {
	// r, err := tx.Stmt(stmt).QueryContext(ctx, args...)
	r, err := tx.Stmt(stmt).QueryContext(ctx, args...)
	defer r.Close()
	if err != nil {
		return false, err
	}
	return r.Next(), nil
}

func UpdateRow(ctx context.Context, tx *sql.Tx, stmt *sql.Stmt, args ...interface{}) error {
	return execRow(ctx, tx, stmt, args...)
}

func DeleteRow(ctx context.Context, tx *sql.Tx, stmt *sql.Stmt, args ...interface{}) error {
	return execRow(ctx, tx, stmt, args...)
}

func execRow(ctx context.Context, tx *sql.Tx, stmt *sql.Stmt, args ...interface{}) error {
	r, err := tx.Stmt(stmt).ExecContext(ctx, args...)
	if err != nil {
		return err
	}
	ra, err := r.RowsAffected()
	if err != nil {
		return err
	}
	if ra != 1 {
		return ErrOnlyOneRowMustBeAffected
	}
	return nil
}
