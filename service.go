package ms

import (
	"context"
	"database/sql"
	"log"
	"time"
)

func OnTx(ctx context.Context, timeout time.Duration, db *sql.DB, i sql.IsolationLevel, readOnly bool, f func(tx *sql.Tx) error) error {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	tx, err := db.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelReadUncommitted, ReadOnly: false})
	if err != nil {
		return err
	}

	if err := f(tx); err != nil {
		rollbackSoft(tx)
		return err
	}

	if err := tx.Commit(); err != nil {
		rollbackSoft(tx)
		return err
	}

	return nil
}

func OnTxStream(ctx context.Context, timeout time.Duration, db *sql.DB, i sql.IsolationLevel, readOnly bool, f func(tx *sql.Tx) (chan interface{}, error)) (chan interface{}, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	tx, err := db.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelReadUncommitted, ReadOnly: false})
	if err != nil {
		return nil, err
	}

	c, err := f(tx)
	if err != nil {
		rollbackSoft(tx)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		rollbackSoft(tx)
		return nil, err
	}

	return c, nil
}

func GetOnTx(ctx context.Context, timeout time.Duration, db *sql.DB, i sql.IsolationLevel, readOnly bool, f func(tx *sql.Tx) (interface{}, error)) (interface{}, error) {
	ctx, cancel := context.WithTimeout(ctx, timeout)
	defer cancel()

	tx, err := db.BeginTx(ctx, &sql.TxOptions{Isolation: sql.LevelReadUncommitted, ReadOnly: false})
	if err != nil {
		return nil, err
	}

	v, err := f(tx)
	if err != nil {
		rollbackSoft(tx)
		return nil, err
	}

	if err := tx.Commit(); err != nil {
		rollbackSoft(tx)
		return nil, err
	}

	return v, nil
}

func rollbackSoft(tx *sql.Tx) {
	if err := tx.Rollback(); err != nil && err != sql.ErrTxDone {
		log.Println(err)
	}
}
