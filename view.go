package ms

import (
	"encoding/base64"
	"encoding/json"
	"errors"
	"net/http"
	"reflect"

	"gitlab.com/gopherburrow/mux"
)

var (
	ErrNotFuncHandlerTriplet = errors.New("ms: Not a Mux Handler triplets N*(URI, Http Method, http.HandlerFunc)")
)

type ReqValues []interface{}

type Errors map[error]Error

type Error struct {
	ID     string `json:"id,omitempty"`
	Code   string `json:"code,omitempty"`
	Title  string `json:"title,omitempty"`
	Status int    `json:"status,omitempty,string"`
	Detail string `json:"detail,omitempty"`
	//Links: a links object containing the following members:
	//  about: a link that leads to further details about this particular occurrence of the problem.
	//source: an object containing references to the source of the error, optionally including any of the following members:
	//pointer: a JSON Pointer [RFC6901] to the associated entity in the request document [e.g. "/data" for a primary data object, or "/data/attributes/title" for a specific attribute].
	//parameter: a string indicating which URI query parameter caused the error.
	//meta: a meta object containing non-standard meta-information about the error.
}

type Response struct {
	Data  interface{} `json:"data,omitempty"`
	Error *Error      `json:"error,omitempty"`
}

func Values(values ...interface{}) ReqValues {
	return values
}

func PrepareMux(m *mux.Mux, muxHandleTriplets ...interface{}) error {
	l := len(muxHandleTriplets)
	if l%3 != 0 {
		return ErrNotFuncHandlerTriplet
	}
	for i := 0; i < l; i += 3 {
		if _, ok := muxHandleTriplets[i].(string); !ok {
			return ErrNotFuncHandlerTriplet
		}
		if _, ok := muxHandleTriplets[i+1].(string); !ok {
			return ErrNotFuncHandlerTriplet
		}
		if _, ok := muxHandleTriplets[i+2].(func(http.ResponseWriter, *http.Request)); !ok {
			return ErrNotFuncHandlerTriplet
		}
	}

	for i := 0; i < l; i += 3 {
		uri, _ := muxHandleTriplets[i].(string)
		method, _ := muxHandleTriplets[i+1].(string)
		handlerFunc, _ := muxHandleTriplets[i+2].(func(http.ResponseWriter, *http.Request))
		if err := m.Handle(method, uri, http.HandlerFunc(handlerFunc)); err != nil {
			return err
		}
	}

	return nil
}

func SetValues(w http.ResponseWriter, r *http.Request, m *mux.Mux, values ...interface{}) error {
	pv := m.PathValues(r)
	vi := 0
	for _, v := range pv {
		if cv, ok := values[vi].(*string); ok {
			*cv = v
			vi++
			continue
		}
		if cv, ok := values[vi].(*[]byte); ok {
			*cv = make([]byte, base64.RawURLEncoding.DecodedLen(len(v)))
			if _, err := base64.RawURLEncoding.Decode(*cv, []byte(v)); err != nil {
				//TODO Error handling
				//ms.JsonError(w, err.Error(), http.StatusBadRequest)
				http.Error(w, err.Error(), http.StatusBadRequest)
				return err
			}
			vi++
			continue
		}
		//TODO Error handling
		//ms.JsonError(w, err.Error(), http.StatusInternalServerError)
		http.Error(w, "TODO", http.StatusInternalServerError)
		return errors.New("TODO")
	}

	for vi < len(values) {
		if vi == len(values)-1 {
			if reflect.ValueOf(values[vi]).Kind() != reflect.Ptr {
				//TODO Error handling
				//ms.JsonError(w, err.Error(), http.StatusInternalServerError)
				http.Error(w, "TODO", http.StatusInternalServerError)
				return errors.New("TODO")
			}
			b := r.Body
			defer b.Close()
			d := json.NewDecoder(b)
			d.DisallowUnknownFields()
			if err := d.Decode(values[vi]); err != nil {
				//TODO Error handling
				//ms.JsonError(w, err.Error(), http.StatusBadRequest)
				http.Error(w, err.Error(), http.StatusBadRequest)
				return err
			}
			return nil
		}
		queryParamName, ok := values[vi].(string)
		if !ok {
			//TODO Error handling
			//ms.JsonError(w, err.Error(), http.StatusInternalServerError)
			http.Error(w, "TODO", http.StatusInternalServerError)
			return errors.New("TODO")
		}

		vi++
		queryParamValue := r.URL.Query().Get(queryParamName)
		if cv, ok := values[vi].(*string); ok {
			*cv = queryParamValue
			vi++
			continue
		}
		if cv, ok := values[vi].(*[]byte); ok {
			*cv = make([]byte, base64.RawURLEncoding.DecodedLen(len(queryParamValue)))
			if _, err := base64.RawURLEncoding.Decode(*cv, []byte(queryParamValue)); err != nil {
				//TODO Error handling
				//ms.JsonError(w, err.Error(), http.StatusBadRequest)
				http.Error(w, err.Error(), http.StatusBadRequest)
				return err
			}
			vi++
			continue
		}

		//TODO Error handling
		//ms.JsonError(w, err.Error(), http.StatusInternalServerError)
		http.Error(w, "TODO", http.StatusInternalServerError)
		return errors.New("TODO")
	}
	return nil
}

func Service(w http.ResponseWriter, r *http.Request, v ReqValues, e Errors, serviceCall func() (interface{}, error)) {
	m, err := mux.Get(r)
	if err != nil {
		//TODO ?
		return
	}
	h := w.Header()
	h.Set("Content-Type", "application/vnd.api+json")
	if err := SetValues(w, r, m, v...); err != nil {
		//TODO ?
		return
	}
	entity, err := serviceCall()
	status := http.StatusOK
	resp := &Response{}
	if err == nil {
		resp.Data = entity
	} else {
		status = http.StatusInternalServerError
		jsonErr, ok := e[err]
		if !ok {
			jsonErr = Error{Title: err.Error()}
		}
		if jsonErr.Status != 0 {
			status = jsonErr.Status
		}
		resp.Error = &jsonErr
	}
	w.WriteHeader(status)
	enc := json.NewEncoder(w)
	enc.Encode(resp)
	return
}
